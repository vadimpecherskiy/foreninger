import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-forening-list',
  templateUrl: './forening-list.component.html',
  styleUrls: ['./forening-list.component.css']
})
export class ForeningListComponent implements OnInit {

  isCache = false;
  data:ListData;
  searchForm;
  types:ForenType[];
  currentType;

  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder,
  ) {
    this.searchForm = this.formBuilder.group({
      type: '',
    });
  }

  ngOnInit() {
    this.loadMyData(null);
    this.loadMyConfigData();
  }

  loadMyData(searchData) {
    //this.data = null;
    this.currentType = searchData != null ? searchData.type : null;
    var url = searchData != null && searchData.nextUrl != null
      ? searchData.nextUrl
      : (this.isCache
          ? (this.currentType ? "/assets/foreningerTypeTemp.json" : "/assets/foreningerTemp.json")
          : ("https://www.dnt.no/api/v3/iprospect_pilot/foreninger/?format=json" + (this.currentType ? "&type=" + this.currentType : ""))
        );
    console.log("load", searchData, url);
    return this.http.get<ListData>(url) //foreningerTemp test
      .subscribe((d:ListData) => {
        //console.log('loaded', d);
        this.data = { ...d };
      });
  }

  loadMyConfigData() {
    return this.http.get<ForenType[]>('/assets/foreningTypes.json')
      .subscribe((arr:ForenType[]) => {
        this.types = arr;
      });
  }

  onSubmit(searchData) {
    console.log('Search form', searchData);
    this.loadMyData(searchData);
    //this.searchForm.reset();
  }

  search(type, nextUrl) {
    console.log('search', type, nextUrl);
    this.loadMyData({type:type, nextUrl:nextUrl});
  }

}

export interface ListData {
  count;
  next;
  results: ForenData[];
}

export interface ForenData {
  id,
  name,
  type,
  typeName,
  url
}

export interface ForenType {
  name,
  type
}
