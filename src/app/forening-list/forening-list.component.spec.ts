import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForeningListComponent } from './forening-list.component';

describe('ForeningListComponent', () => {
  let component: ForeningListComponent;
  let fixture: ComponentFixture<ForeningListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForeningListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForeningListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
