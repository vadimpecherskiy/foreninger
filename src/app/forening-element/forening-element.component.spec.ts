import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForeningElementComponent } from './forening-element.component';

describe('ForeningElementComponent', () => {
  let component: ForeningElementComponent;
  let fixture: ComponentFixture<ForeningElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForeningElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForeningElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
