import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-forening-element',
  templateUrl: './forening-element.component.html',
  styleUrls: ['./forening-element.component.css']
})
export class ForeningElementComponent implements OnInit {

  @Input() item;
  @Output() notify = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  searchByType(type:string) {
    this.notify.emit();
  }
}
