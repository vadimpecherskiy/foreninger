import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ForeningListComponent } from './forening-list/forening-list.component';
import { ForeningElementComponent } from './forening-element/forening-element.component';

@NgModule({
  imports: [
    BrowserModule,
    // import HttpClientModule after BrowserModule.
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', component: ForeningListComponent },
      { path: 'search/:type', component: ForeningListComponent },
    ])
  ],
  declarations: [
    AppComponent,
    ForeningListComponent,
    ForeningElementComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
